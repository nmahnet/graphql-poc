package net.croz.graphqlpoc.controller;

import io.leangen.graphql.spqr.spring.web.dto.GraphQLRequest;
import net.croz.graphqlpoc.service.GraphQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by nmahnet on 22.11.2021.
 */
@ConditionalOnProperty(name = "graphql.spqr.http.enabled", havingValue = "false")
@RestController
public class GraphQLController {

    private final GraphQLService graphQLService;

    @Autowired
    public GraphQLController(GraphQLService graphQLService) {
        this.graphQLService = graphQLService;
    }

    @PostMapping(value = "/graphql", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> indexFromAnnotated(@RequestBody GraphQLRequest request, HttpServletRequest raw) {
        Map<String, Object> result = graphQLService.indexFromAnnotated(request, raw);
        return ResponseEntity.ok(result);
    }
}
