package net.croz.graphqlpoc.repository;

import net.croz.graphqlpoc.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by nmahnet on 22.11.2021.
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
}
