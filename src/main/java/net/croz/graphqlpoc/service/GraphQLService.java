package net.croz.graphqlpoc.service;

import io.leangen.graphql.spqr.spring.web.dto.GraphQLRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by nmahnet on 22.11.2021.
 */
public interface GraphQLService {

    Map<String, Object> indexFromAnnotated(GraphQLRequest request, HttpServletRequest raw);
}
