package net.croz.graphqlpoc.service;

import net.croz.graphqlpoc.model.Author;

import java.util.List;
import java.util.Optional;

/**
 * Created by nmahnet on 22.11.2021.
 */
public interface AuthorService {

    Optional<Author> findById(Long id);

    List<Author> findAll();
}
