package net.croz.graphqlpoc.service;

import net.croz.graphqlpoc.model.Book;

import java.util.List;
import java.util.Optional;

/**
 * Created by nmahnet on 22.11.2021.
 */
public interface BookService {

    Optional<Book> findById(Long id);

    List<Book> findAll();
}
