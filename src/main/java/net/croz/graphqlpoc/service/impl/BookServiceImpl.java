package net.croz.graphqlpoc.service.impl;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import net.croz.graphqlpoc.model.Book;
import net.croz.graphqlpoc.repository.BookRepository;
import net.croz.graphqlpoc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by nmahnet on 22.11.2021.
 */
@Service
@GraphQLApi
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    @GraphQLQuery(name = "findBookById")
    public Optional<Book> findById(@GraphQLArgument(name = "id") Long id) {
        return bookRepository.findById(id);
    }

    @Override
    @GraphQLQuery(name = "findAllBooks")
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

}
