package net.croz.graphqlpoc.service.impl;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.leangen.graphql.spqr.spring.web.dto.GraphQLRequest;
import net.croz.graphqlpoc.service.AuthorService;
import net.croz.graphqlpoc.service.BookService;
import net.croz.graphqlpoc.service.GraphQLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by nmahnet on 22.11.2021.
 */
@Service
public class GraphQLServiceImpl implements GraphQLService {

    private final GraphQL graphQL;

    @Autowired
    public GraphQLServiceImpl(AuthorService authorService,
                              BookService bookService) {
        GraphQLSchema schema = new GraphQLSchemaGenerator()
                .withBasePackages("net.croz.graphqlpoc.model")
                .withOperationsFromSingletons(authorService, bookService)
                .generate();
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    @Override
    public Map<String, Object> indexFromAnnotated(GraphQLRequest request, HttpServletRequest raw) {
        ExecutionResult executionResult = graphQL.execute(ExecutionInput.newExecutionInput()
                .query(request.getQuery())
                .context(raw)
                .build());
        return executionResult.toSpecification();
    }
}
