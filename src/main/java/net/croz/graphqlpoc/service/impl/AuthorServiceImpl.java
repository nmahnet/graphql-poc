package net.croz.graphqlpoc.service.impl;

import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import net.croz.graphqlpoc.model.Author;
import net.croz.graphqlpoc.repository.AuthorRepository;
import net.croz.graphqlpoc.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by nmahnet on 22.11.2021.
 */
@Service
@GraphQLApi
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    @GraphQLQuery(name = "findAuthorById")
    public Optional<Author> findById(@GraphQLArgument(name = "id") Long id) {
        return authorRepository.findById(id);
    }

    @Override
    @GraphQLQuery(name = "findAllAuthors")
    public List<Author> findAll() {
        return authorRepository.findAll();
    }
}
